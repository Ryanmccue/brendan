# README #

### Overview ###
For your hiring project, you will make a web app which books meetings in a room.

### Requirements ###

* Email/password authentication with FireBase.
* Facebook authentication with Firebase
* Frontend must be written using React.
* Frontend styling must be Material Design.
* User can not schedule a meeting in an overlapping timeslot.

### Views ###

* Login view
* Register view
* Home page
* Agenda calender view (Show upcoming meetings broken up by day)
* Daily calender view (Agenda view for the day)
* Weekly calender view (Agenda view for the week)

### BONUS ###

Allow meeting creators to invite other users to the meeting.

### Next Steps ###

* If you don't fully grasp how the Firebase database and queries work this is a good video series called `The Firebase Database For SQL Developers` (https://www.youtube.com/playlist?list=PLl-K7zZEsYLlP-k-RKFa7RyNPa9_wCH2s)
* Create a Firebase project (https://firebase.google.com/)
* Start coding
* Make commits in the repository.

If you have any questions email me at ryanm@freshworks.io!