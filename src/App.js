import React, { Component } from 'react';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import firebase from 'firebase';

import { Route, withRouter } from 'react-router'
import { routerReducer, push, routerMiddleware, ConnectedRouter } from 'react-router-redux';

import './styles/App.scss';

import Nav from './components/Nav';
import Welcome from './components/Welcome';
import Login from './components/Login';
import Register from './components/Register';
import Home from './components/Home';
import Calendar from './components/Calendar';
import Toast from './components/Toast';
import { firebaseAuth } from './services/Firebase';
import { authStateChanged } from './actions/actionRequests/actionRequests';


injectTapEventPlugin();


class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      authed: false
    }
  }

  componentDidMount() {
    this.removeListener = firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.setState({
          user: null,
          authed: false
        });
      } else {
        this.setState({
          user: user,
          authed: true
        });
      }
    });
  }

  componentWillUnmount() {
    this.removeListener();
  }


  render() {

    return this.props.loading === true ? <h1>Loading</h1> : (
      <MuiThemeProvider>
        <div className='container'>
          <div className='row'>
            <Nav history={this.props.history} user={this.state.user} authed={this.state.authed}/>
            <ConnectedRouter history={this.props.history}>
              <div>
                <Route exact path='/' component={withRouter(Welcome)} />
                <Route path='/login' component={withRouter(Login)} />
                <Route path='/register' component={withRouter(Register)} />
                <Route path='/home' component={withRouter(Home)} />
                <Route path='/calendar' component={withRouter(Calendar)} />
                <Toast />
              </div>
            </ConnectedRouter>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default connect (
  state => ({
    router: state.router,
    success: !state.rootReducer.authenticate.didFail,
    loading: state.rootReducer.authenticate.isFetching,
    user: state.rootReducer.authenticate.user,
  }),
  dispatch => ({})
)(App)
