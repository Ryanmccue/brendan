export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const REQUEST_LOGIN_FAILURE = 'REQUEST_LOGIN_FAILURE';
export const REQUEST_LOGIN_SUCCESS = 'REQUEST_LOGIN_SUCCESS';

export const REQUEST_PASSWORD_RESET = 'REQUEST_PASSWORD_RESET';

export const REQUEST_AUTH = 'REQUEST_AUTH';
export const REQUEST_AUTH_SUCCESS = 'REQUEST_AUTH_SUCCESS';
export const REQUEST_AUTH_FAILURE = 'REQUEST_AUTH_FAILURE';

export const REQUEST_TOAST_CLOSE = 'REQUEST_TOAST_CLOSE';
export const REQUEST_TOAST_OPEN = 'REQUEST_TOAST_OPEN';

