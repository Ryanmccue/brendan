import fetch from 'isomorphic-fetch';
import firebase from 'firebase';
import { push } from 'react-router-redux';

import {
    requestLogin, 
    requestLoginSuccess, 
    requestPasswordReset,
    requestLoginFailure, 
    requestAuth, 
    requestAuthFailure, 
    requestAuthSuccess,
    requestToastClose,
    requestToastOpen
} from '../actions';


export const login = (email, pass) => {
    return dispatch => {
        //loading done here
        // to push to new areas -> dispatch(push('/login'));
        dispatch(requestLogin());
        firebase.auth().signInWithEmailAndPassword(email, pass).then((data) => {
            dispatch(requestLoginSuccess(data));
        }).catch( (error) => {
            let errorMessage = error.message;
            dispatch(toastOpen(errorMessage));
            dispatch(requestLoginFailure());
        });
    }
}

export const loginWithFacebook = () => {
    return dispatch => {const provider = new firebase.auth.FacebookAuthProvider();
        dispatch(requestLogin());
        firebase.auth().signInWithPopup(provider).then((data) => {
            let token = data.credential.accessToken;
            dispatch(requestLoginSuccess(data));
        }).catch( (error) => {
            let errorMessage = error.message;
            dispatch(requestLoginFailure());
            dispatch(toastOpen(errorMessage));
        });
    }
}

// needs tweaking
export const passwordReset = (email) => {
    return dispatch => {
        dispatch(requestPasswordReset());
        firebase.auth().sendPasswordResetEmail(email);
    }
}

export const createUser = (email, pass) => {
    return dispatch => {
        const ref = firebase.database().ref();
        dispatch(requestAuth());
        firebase.auth().createUserWithEmailAndPassword(email, pass).then((user) => {
            ref.child(`users/${user.uid}`)
                .set({
                    email: user.email,
                    uid: user.uid
                }).then(() => user);
        }).then((user) => {
            dispatch(requestAuthSuccess(user));
        }).catch((error) => {
            let errorMessage = error.message;
            dispatch(requestAuthFailure());
            dispatch(toastOpen(errorMessage));
        });
    }
}

export const authStateChanged = () => {
    return dispatch => {
        dispatch(requestAuth());
        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                //handle state change
                dispatch(requestAuthSuccess(user));
            } else {
                //handle auth error
                let error = 'Authentication Failed'
                dispatch(requestAuthFailure());
                dispatch(toastOpen(error));
            }
        })
    }
}

export const toastClose = () => dispatch => {
    dispatch(requestToastClose());
}

export const toastOpen = (error) => dispatch => {
    dispatch(requestToastOpen(error));
}
