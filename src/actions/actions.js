import { 
  REQUEST_LOGIN,
  REQUEST_LOGIN_FAILURE, 
  REQUEST_LOGIN_SUCCESS,
  REQUEST_AUTH,
  REQUEST_AUTH_FAILURE,
  REQUEST_AUTH_SUCCESS,
  REQUEST_PASSWORD_RESET,
  REQUEST_TOAST_CLOSE,
  REQUEST_TOAST_OPEN
} from '../actionTypes/actionTypes.js';


export const requestLogin = () => {
  return {
    type: REQUEST_LOGIN,
  }
}

export const requestPasswordReset = () => {
  return {
    type: REQUEST_PASSWORD_RESET
  }
}

export const requestLoginSuccess = (json) => {
  return {
    type: REQUEST_LOGIN_SUCCESS,
    data: json
  }
}

export const requestLoginFailure = (error) => {
  return {
    type: REQUEST_LOGIN_FAILURE,
    error: error
  }
}

export const requestAuth = () => {
  return {
    type: REQUEST_AUTH
  }
}

export const requestAuthFailure = (error) => {
  return {
    type: REQUEST_AUTH_FAILURE,
    error: error
  }
}

export const requestAuthSuccess = (user) => {
  return {
    type: REQUEST_AUTH_SUCCESS,
    data: user
  }
}

export const requestToastClose = () => {
  return {
    type: REQUEST_TOAST_CLOSE,
    showToast: false,
    toastMessage: ''
  }
}

export const requestToastOpen = (error) => {
  return {
    type: REQUEST_TOAST_OPEN,
    showToast: true,
    toastMessage: error
  }
}
