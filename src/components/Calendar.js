import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';


import { getUserEvents, getAllEvents, deleteMyEvent } from '../events/userEvents';
import { ref } from '../services/Firebase';

import '.././styles/react-big-calendar.scss';


BigCalendar.momentLocalizer(moment);


class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openAgain: false,
      title: '',
      desc: '',
      start: null,
      end: null,
      events: [],
      uid: null
    }
    this.getMyEvents.bind(this);
    this.retrieveAllEvents.bind(this);
  }

  handleOpen(slotInfo) {
    let items = this.state.events.map((item) => {
      if(item.start <= slotInfo.end && item.end >= slotInfo.start) {
        return 'exists';
      }
    });
    if(!items.includes('exists')) {
      this.setState({open: true});
      this.setState({
        start: slotInfo.start.toLocaleString(),
        end: slotInfo.end.toLocaleString()
      });
    } 
  };

  handleOpenAgain(event) {
    this.setState({
      openAgain: true, 
      start: event.start.toLocaleString(), 
      end: event.end.toLocaleString(), 
      desc: event.desc, 
      title: event.title,
      uid: event.uid
    });
  }

  handleClose(event) {
    if (this.refs.loaded) {
      this.setState({open: false});
      if (this.state.title == '' || this.state.desc == '') {
        this.setState({title: '', start: null, end: null});
        return;
      } else {
        const newEventData = {
          title: this.state.title,
          desc: this.state.desc,
          start: this.state.start,
          end: this.state.end,
          uid: this.props.user.uid
        };
        let newEventKey = ref.child('events').push().key;
        let updates = {};
        updates['/events/' + newEventKey] = newEventData;
        updates['/user-events/' + this.props.user.uid + '/' + newEventKey] = newEventData;
        this.setState({title: '', desc: '', start: null, end: null});
        return ref.update(updates);
      }
    }
  };

  handleCloseAgain() {
    this.setState({
      openAgain: false,
      start: null,
      end: null,
      desc: '',
      title: '',
      uid: null
    });
  }
  
  handleChange(event) {
    this.setState({
      title: event.target.value
    });
  };

  handleDescription(event) {
    this.setState({
      desc: event.target.value
    });
  }
  
  getMyEvents() {
    let newArray = [];
    getUserEvents(this.props.user.uid, snap => {
      newArray[0] = snap.val();
      this.organizeEvents(newArray[0]);
    });
  }

  retrieveAllEvents() {
    let newArray = [];
    newArray = getAllEvents();
    this.organizeEvents(newArray[0]);
  }

  deleteEvent() {
    if (this.props.user.uid == this.state.uid) {
      deleteMyEvent(this.state.start);
      this.setState({
        openAgain: false,
        start: null,
        end: null,
        desc: '',
        title: '',
        uid: null
      });
      this.getMyEvents();
    } else {
      return;
    }
  }

  componentDidMount() {
    if (this.refs.loaded) {
      this.getMyEvents();
    }
  }

  componentWillUnmount() {
    console.log('unmounted');
  }

  organizeEvents(event) {
    //get key and then use that key to reference the item
    let events = [];
    for (let item in event) {
      let i = event[item];
      events.push(i);
    }
    let organizedArray = [];
    for (let e in events) {
      let title = events[e].title;
      let startDate = new Date(moment(Date.parse(events[e].start)).format('MM-DD-YYYY HH:mm'));
      let endDate = new Date(moment(Date.parse(events[e].end)).format('MM-DD-YYYY HH:mm'));
      let desc = events[e].desc;
      let uid = events[e].uid;
      organizedArray.push({
        'title': title,
        'start': startDate,
        'end': endDate,
        'desc': desc,
        'uid': uid
      });
    }
    this.setState({
      events: organizedArray
    });
  }

  render () {
    const actions = [
      <TextField
        name='description-input'
        floatingLabelText='Description'
        value={this.state.description}
        onChange={this.handleDescription.bind(this)}
      />,
      <TextField
        name='title-input'
        floatingLabelText='Title'
        value={this.state.title}
        onChange={this.handleChange.bind(this)}
      />,
      <FlatButton
        className='cancel-button'
        label='Cancel'
        primary={false}
        onTouchTap={this.handleClose.bind(this)}
      />,
      <FlatButton
        className='submit-booking'
        label='Submit'
        primary={true}
        onTouchTap={this.handleClose.bind(this)}
      />
    ];

    const moreActions = [
      <FlatButton
        className='cancel-button'
        label='Cancel'
        primary={false}
        onTouchTap={this.handleCloseAgain.bind(this)}
      />,
      <FlatButton
        className='delete-booking'
        label='Delete'
        primary={true}
        onTouchTap={this.deleteEvent.bind(this)}
      />
    ];

    return (
      <div ref='loaded'>
        <Dialog
          title={this.state.title}
          actions={actions}
          modal={true}
          open={this.state.open}
        >
          <p>{this.state.desc}</p>
          <p>{this.state.start} - {this.state.end}</p>
          <p>at FreshWorks Studio, 736 Broughton Street</p>
        </Dialog>
        <Dialog
          title={this.state.title}
          actions={moreActions}
          modal={true}
          open={this.state.openAgain}
        >
          <p>{this.state.desc}</p>
          <p>{this.state.start} - {this.state.end}</p>
          <p>at FreshWorks Studio, 736 Broughton Street</p>
        </Dialog>
        <FlatButton 
          label='My Bookings'
          primary={true}
          onTouchTap={this.getMyEvents.bind(this)}
        />
        <FlatButton
          label='All Bookings'
          primary={true}
          onTouchTap={this.retrieveAllEvents.bind(this)}
        />
        <BigCalendar
          selectable
          events={this.state.events}
          defaultView='week'
          scrollToTime={new Date(1970, 1, 1, 6)}
          defaultDate={new Date()}
          onSelectEvent={(event) => this.handleOpenAgain(event)}
          onSelectSlot={(slotInfo) => this.handleOpen(slotInfo)}
        />
      </div>
    );
  }
}

export default Calendar;
