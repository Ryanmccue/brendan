import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';


class Home extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className='home-page'>
       <h1>One Room Booker App</h1>
       <img className='home-logo' src='../images/oneLogo.png'/>
       <p>Created by Brendan Walker</p>
       <img src='../images/how-to-book.gif' className='how-to' />
       <p>How to create a booking <i className="material-icons">arrow_upward</i></p>
       <img src='../images/how-to-delete.gif' className='how-to' />
       <p>How to delete a booking <i className="material-icons">arrow_upward</i></p>
       <img src='../images/cannot-overlap.gif' className='how-to' />
       <p><i className="material-icons">warning</i> You cannot overlap a booking <i className="material-icons">arrow_upward</i></p>
       <p className='start-booking'>Start booking today!</p>
       <Link to='/calendar' className='calendar-button calendar-home'><RaisedButton><i className="material-icons nav-icons">event_note</i></RaisedButton></Link>
      </div>
    )
  }
}

export default Home;
