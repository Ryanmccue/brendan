import React, { Component } from 'react';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import { login, loginWithFacebook, authStateChanged, passwordReset } from '../actions/actionRequests/actionRequests';


class Login extends Component {
  constructor(props) {
    super(props);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.login(this.email.getValue(), this.pass.getValue());
  }

  resetPassword() {
    this.props.passwordReset(this.email.getValue());
  }

  render() {

    return (
      <div className='login-form'>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <TextField
              name='email-input' 
              ref={(email) => this.email = email}
              floatingLabelText='Email'
              type='email'
            />
          </div>
          <div>
            <TextField 
              name='password-input'
              ref={(pass) => this.pass = pass}
              floatingLabelText='Password'
              type='password'
            />
          </div>
          <RaisedButton
            className='submit-button' 
            type='submit'
            label='Submit'
            primary={true}
          />
            <div>
              <a href="#" onClick={this.resetPassword.bind(this)}>Forgot Password?</a>
            </div>
        </form>
        <h3>Log in with Facebook</h3>
        <button className='facebook-button' onClick={this.props.loginWithFacebook}><i className="fa fa-facebook-square" aria-hidden="true"></i> Facebook</button>
      </div>
    );
  }
}

export default connect (
  state => ({
    error: state.rootReducer.authenticate.error,
    success: !state.rootReducer.authenticate.didFail,
    loading: state.rootReducer.authenticate.isFetching,
    user: state.rootReducer.authenticate.user,
  }),
  dispatch => ({
    login: (email, password) => {
      dispatch(login(email, password));
    },
    loginWithFacebook: () => {
      dispatch(loginWithFacebook());
    },
    passwordReset: () => {
      dispatch(passwordReset());
    },
    authStateChanged: () => {
      dispatch(authStateChanged());
    }
  })
)(Login);
