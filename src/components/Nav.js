import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Router } from 'react-router';
import { Link } from 'react-router-dom';
import firebase from 'firebase';

import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { AppBar } from 'material-ui';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';


class Nav extends Component {

  loggingOut() {
    firebase.auth().signOut();
  }

  home() {
    this.props.history.push('/home');
  }

  calendar() {
    this.props.history.push('/calendar');
  }

  render () {

    return (
      <AppBar title='One Room Booker App' iconElementRight={this.props.authed
        ? 
        <span>
          <span className='user-email'>{this.props.user.email}</span>
          <span className='home-button'><RaisedButton onTouchTap={this.home.bind(this)}><i className="material-icons nav-icons">home</i></RaisedButton></span>
          <span className='calendar-button'><RaisedButton onTouchTap={this.calendar.bind(this)}><i className="material-icons nav-icons">event_note</i></RaisedButton></span>
          <FlatButton
            className='logout-button'
            label='Logout'
            primary={false}
            onTouchTap={this.loggingOut.bind(this)}
          />
        </span>
        : 
        <span>
          <span><FlatButton label='Login'/></span>
          <span><FlatButton label='Register'/></span>
        </span>
        } iconElementLeft={<span><img className='logo' src='../images/oneLogo.png'/></span>}
        iconStyleRight={{margin: '10px'}} iconStyleLeft={{marginTop: '10px'}}>
      </AppBar>
    );
  }
}

export default connect (
    state => ({
      router: state.router,
      success: !state.rootReducer.authenticate.didFail,
      loading: state.rootReducer.authenticate.isFetching,
    }),
    dispatch => ({
      logout: () => {
        dispatch(logout());
      }
    })
)(Nav);
