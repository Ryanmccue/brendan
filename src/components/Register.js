import React, { Component } from 'react';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';

import { createUser } from '../actions/actionRequests/actionRequests';


class Register extends Component {

  constructor(props) {
    super(props);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.createUser(this.email.getValue(), this.pass.getValue());
  }


  render() {
    return (
      <div className="register-form">
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <TextField 
              name='email-input'
              ref={(email) => this.email = email}
              floatingLabelText='Email'
              type='email'
            />
          </div>
          <div>
            <TextField
              name='password-input' 
              ref={(pass) => this.pass = pass}
              floatingLabelText='Password'
              type='password'
            />
          </div>
          <RaisedButton 
            className='submit-button'
            type='submit'
            label='Submit'
            primary={true}
          />
        </form>
      </div>
    )
  }
}

export default connect (
    state => ({
      error: state.rootReducer.authenticate.error,
      success: !state.rootReducer.authenticate.didFail,
      loading: state.rootReducer.authenticate.isFetching,
      user: state.rootReducer.authenticate.user,
    }),
    dispatch => ({
      createUser: (email, password) => {
        dispatch(createUser(email, password));
      }
    })
)(Register);
