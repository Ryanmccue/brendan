import React, { Component } from 'react';
import Snackbar from 'material-ui/Snackbar';
import { connect } from 'react-redux';
import { toastClose, toastOpen } from '../actions/actionRequests/actionRequests';

class Toast extends React.Component {
  constructor(props) {
    super(props);
  }

  closeToast() {
    this.props.toastClose();
  }

  render() {
    return (
      <Snackbar
        open={this.props.showToast === undefined ? false : this.props.showToast}
        message={this.props.toastMessage === undefined ? '' : this.props.toastMessage}
        onRequestClose={() => this.closeToast()}
        autoHideDuration={4000}
        action='Close'
        onActionTouchTap={() => this.closeToast()}
      />
    );
  }
}

export default connect(
  state => ({
    toastMessage: state.rootReducer.toast.toastMessage,
    showToast: state.rootReducer.toast.showToast
  }),
  dispatch => ({
    toastClose: () => {
      dispatch(toastClose());
    },
    toastOpen: () => {
      dispatch(toastOpen());
    }
  })
)(Toast)
