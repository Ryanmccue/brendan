import * as firebase from 'firebase';
import { ref } from '../services/Firebase';


const allEvents = ref.child('events');
const userEvents = ref.child('user-events');
const userData = ref.child('users');


export function getUserEvents(key, cb) {
  userEvents.child(key).on('child_added', snap => {
    let userRef = userEvents.child(key);
    userRef.once('value', cb);
  });
}

export function getAllEvents() {
  let eventsArray = [];
  allEvents.on('value', snap => {
    eventsArray[0] = snap.val();
  });
  return eventsArray;
}

export function deleteMyEvent(data) {
  allEvents.on('child_added', snap => {
    if (snap.val().start == data.toLocaleString()) {
      allEvents.child(snap.key).remove();
    } else {
      return;
    }
  });
  userEvents.on('child_added', snap => {
    snap.forEach((object) => {
      if (object.val().start == data.toLocaleString()) {
        userEvents.child(snap.key).child(object.key).remove();
      } else {
        return;
      }
    });
  });
}



