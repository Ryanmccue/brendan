import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyD2E9-pT8C0EAddgQJw_98n5WfK9cKDv-4",
  authDomain: "one-room-booker-app.firebaseapp.com",
  databaseURL: "https://one-room-booker-app.firebaseio.com",
  projectId: "one-room-booker-app",
  storageBucket: "one-room-booker-app.appspot.com",
  messagingSenderId: "697267251888"
};
  
firebase.initializeApp(config);

export const provider = new firebase.auth.FacebookAuthProvider();
export const ref = firebase.database().ref();
export const firebaseAuth = firebase.auth;